---
layout: markdown_page
title: "Identity data"
---

#### GitLab Identity Data

Data below is as of 2018-12-31

##### Country Specific Data

| Country Information                       | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 535   | 100%        |
| Based in the US                           | 317   | 59.25%      |
| Based in the UK                           | 24    | 4.49%       |
| Based in the Netherlands                  | 20    | 3.74%       |
| Based in Other Countries                  | 174   | 32.52%      |

##### Gender Data

| Gender (All)                              | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 535   | 100%        |
| Men                                       | 403   | 75.33%      |
| Women                                     | 132   | 24.67%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Leadership                      | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 41    | 100%        |
| Men in Leadership                         | 30    | 73.17%      |
| Women in Leadership                       | 11    | 26.83%      |
| Other Gender Identities                   | 0     | 0%          |

| Gender in Development                     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 259   | 100%        |
| Men in Development                        | 217   | 83.78%      |
| Women in Development                      | 42    | 16.22%      |
| Other Gender Identities                   | 0     | 0%          |

##### Race/Ethnicity Data

| Race/Ethnicity (US Only)                  | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 317   | 100%        |
| Asian                                     | 23    | 7.26%       |
| Black or African American                 | 5     | 1.58%       |
| Hispanic or Latino                        | 22    | 6.94%       |
| Native Hawaiian or Other Pacific Islander | 1     | 0.32%       |
| Two or More Races                         | 14    | 4.42%       |
| White                                     | 183   | 57.73%      |
| Unreported                                | 69    | 21.77%      |

| Race/Ethnicity in Development (US Only)   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 102   | 100%        |
| Asian                                     | 9     | 8.82%       |
| Black or African American                 | 2     | 1.96%       |
| Hispanic or Latino                        | 7     | 6.86%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 8     | 7.84%       |
| White                                     | 58    | 56.86%      |
| Unreported                                | 18    | 17.65%      |

| Race/Ethnicity in Leadership (US Only)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 31    | 100%        |
| Asian                                     | 3     | 9.68%       |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 0     | 0.00%       |
| Native Hawaiian or Other Pacific Islander | 1     | 3.23%       |
| Two or More Races                         | 0     | 0.00%       |
| White                                     | 18    | 58.06%      |
| Unreported                                | 9     | 29.03%      |

| Race/Ethnicity (Global)                   | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 218   | 100%        |
| Asian                                     | 22    | 10.09%      |
| Black or African American                 | 6     | 2.75%       |
| Hispanic or Latino                        | 10    | 4.59%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 4     | 1.83%       |
| White                                     | 108   | 49.54%      |
| Unreported                                | 68    | 31.19%      |

| Race/Ethnicity in Development (Global)    | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 156   | 100%        |
| Asian                                     | 16    | 11.11%      |
| Black or African American                 | 5     | 3.03%       |
| Hispanic or Latino                        | 10    | 6.57%       |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 3     | 1.92%       |
| White                                     | 78    | 50.00%      |
| Unreported                                | 44    | 28.21%      |

| Race/Ethnicity in Leadership (Global)     | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 9     | 100%        |
| Asian                                     | 0     | 0.00%       |
| Black or African American                 | 0     | 0.00%       |
| Hispanic or Latino                        | 1     | 11.11%      |
| Native Hawaiian or Other Pacific Islander | 0     | 0.00%       |
| Two or More Races                         | 0     | 0.00%       |
| White                                     | 3     | 33.33%      |
| Unreported                                | 5     | 55.56%      |

##### Age Distribution

| Age Distribution (Global)                 | Total | Percentages |
|-------------------------------------------|-------|-------------|
| Total Team Members                        | 535   | 100%        |
| 18-24                                     | 17    | 3.18%       |
| 25-29                                     | 112   | 20.93%      |
| 30-34                                     | 161   | 30.09%      |
| 35-39                                     | 99    | 18.50%      |
| 40-49                                     | 104   | 19.44%      |
| 50-59                                     | 36    | 6.73%       |
| 60+                                       | 5     | 0.93%       |
| Unreported                                | 1     | 0.19%       |
