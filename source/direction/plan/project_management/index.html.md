---
layout: markdown_page
title: "Category Vision - Project Management"
---

- TOC
{:toc}

## Project Management

Project management in GitLab allows teams to plan, manage, and track work, usually software development work, to create potentially complex applications and services. 

In particular, the primary object leveraged is the issue, which is the single source of truth for a given feature or any change in general, where team members collaborate on ideas and make decisions to move the process forward for that change. Attributes such as labels, milestones, assignees, and weights (story points) provide flexible ways for teams to do the otherwise difficult task of managing a large number of issues. 

[Kanban boards](https://gitlab.com/groups/gitlab-org/-/epics/665) are a powerful way to do that management as well, and it is its own product category. [Agile portfolio management](https://gitlab.com/groups/gitlab-org/-/epics/667) focuses on planning at a higher level work abstraction, and at longer time scales, and is also its own product category. 

## From project to product

The future of project management is helping teams create and maintain long-lasting products that serve their customers. Traditional project management is very much focused on simply managing a project. A project is loosely defined as a set of requirements. And the purpose of project management here is to help teams ensure that the project is completed on time and on budget, based on the original agreed upon plan. (This concept will still be relevant for contracting scenarios. But GitLab is not focused on that use case.) The thinking used to be (and currently still is in many organizations), is that shipping product and features means completing a series of projects. This is an older mindset and GitLab is not focused on this way of helping teams, because we simply believe that there is a better way.

The project mindset suffers from being inward-focused, and puts too much emphasis on making sure plans are adhered to, for the sake of organization and stability within an organization. The future that GitLab is participating in, and inventing, is _product management_. It is a focus on the customer and the business value delivered to them. It is outward-focused. If the process is focused on customers, then optimizations will be biased toward that instead. It gives room for Agile principles to breathe since the focus is not on strict constraints and change management. Nonetheless, we will still address the use cases of regulation, security, and compliance as needed in different industries. These will result in features that are targeted to meet those demands to mitigate _business risk_. But they will not be used to manage _project management risk_, which is the previous model. In summary, the project-to-product transition will allow customer needs and business concerns to _pull_ feature development. GitLab is focused on inventing this future.

Naming-wise, we don't anticipate that the _project management_ terminology to change because it is entrenched in the industry. This means we will have to continue educating our customers, and the industry at large and be intentional in explaining that GitLab project management is actually focused on developing products, and _not_ on managing projects.

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorites for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

Custom fields and custom workflows are a powerful notion to allow teams to support their very unique specific workflows, especially in large enterprises. We are addressing these needs with key-value labels. See https://gitlab.com/groups/gitlab-org/-/epics/728.

As well, a more powerful way to do backlog grooming and prioritization in general is also coming. This is somewhat possible right with issue boards, but we want to enable teams to manage even more issues' priorities altogether, even more easily. See https://gitlab.com/groups/gitlab-org/-/epics/482.

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

The top competitor is Atlassian's Jira. It is by far the dominant and entrenched issue tracking system in the industry, and similar to GitLab, has expanded its offerings over the years. Interestingly to address Jira, we are not only offering competitive features, but also integration with Jira itself into GitLab merge requests (https://gitlab.com/gitlab-org/gitlab-ce/issues/27073), as a way to help customers to eventually make the transition to Jira, for example using an export/import migration strategy. See https://docs.gitlab.com/ee/user/project/issues/csv_import.html

In recent years, there has been many smaller player to enter the project management space, to focus on more niche areas and solve more specific use cases. There are many. Some notable ones are Trello, Asana, Notion, Monday, and even Basecamp.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

Analysts in software project management, and in particular enterprise software management, are going beyond basic project management, as it applies to the software world. It is not sufficient to address this singular space and goal anymore. Analysts are looking at the bigger picture of DevOps, Agile transformation, and delivering business value to customers. In many ways, project management has become a subset of the greater goals of shipping value to customers as fast as possible, being able to get feedback as quickly as possible. So the relevant analyst domains are really enterpise agile planning and value stream management, which are two additional categories:
- https://gitlab.com/groups/gitlab-org/-/epics/667
- https://gitlab.com/groups/gitlab-org/-/epics/668

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

More and more customers are asking for manual prioritization beyond priority labels. That's what https://gitlab.com/groups/gitlab-org/-/epics/482 addresses. Additional, folks need a way to do their own custom workflows with boards. That's what https://gitlab.com/groups/gitlab-org/-/epics/424 accomplishes.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Users in general have been asking for custom fields for a long time, and expect a mature project management tool to have such capability. That's what See https://gitlab.com/groups/gitlab-org/-/epics/728 accomplishes with key-value labels.

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

Teams are struggling with a way to manage priorities manually in GitLab. we have issue boards, but they are not sufficient. We need a way to manage a larger number of issues' priorities all at once, and that's what https://gitlab.com/groups/gitlab-org/-/epics/482 accomplishes.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

The most important thing to move the vision forward is accomplishing custom fields and custom workflows. These are two big enhancements to the base issue paradigm of GitLab thus far.

- https://gitlab.com/groups/gitlab-org/-/epics/728.
- https://gitlab.com/groups/gitlab-org/-/epics/482