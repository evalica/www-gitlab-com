---
layout: markdown_page
title: "Category Vision - Time Tracking"
---

- TOC
{:toc}

## Time Tracking
<!-- A good description of what your category is. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. -->

Time tracking allows teams to plan, track, and evaluate (after the fact) time spent by individuals and teams on work. It is an important category since in the world of digital work (where GitLab is a tool for), an organization's ultimate resource is ultimately people hours.

## What's next & why
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in
the category.-->

GitLab has basic time tracking functionality (estimation and actuals) for issues and merge requests. This forms the baseline layer of project management functionality relevant to time tracking. There are several possible directions for time tracking.

- Continue to build out project management functionality, such as time tracking for individuals. See https://gitlab.com/gitlab-org/gitlab-ce/issues/27869.
- Integrate with various existing parts of GitLab, such as commits (https://gitlab.com/gitlab-org/gitlab-ce/issues/42540) and issue boards (https://gitlab.com/gitlab-org/gitlab-ce/issues/27780).
- Expand time tracking to more general areas of project and portfolio management, such as effort/weight estimation, capacity/velocity tracking, and financial management.

Our next steps is to consider quick wins such the first two items above, before tackling more ambitious goals in time tracking (third bullet point).

## Competitive landscape
<!-- The top two or three competitors, and what the next one or two items we should
work on to displace the competitor at customers, ideally discovered through
[customer meetings](https://about.gitlab.com/handbook/product/#customer-meetings). We’re not aiming for feature parity
with competitors, and we’re not just looking at the features competitors talk
about, but we’re talking with customers about what they actually use, and
ultimately what they need.-->

Time tracking is a critical set of features in project management and more broadly, portfolio management. So the main competitors here are similar to those categories. It is Jira, Asana, Notion, and Monday.

GitLab will win by not just having similar time tracking capabilities (which is not very differentiated on its own), but by taking those time tracking capabilities (and generated data), and plugging them into the larger project management feature set used by teams.

## Analyst landscape
<!-- What analysts and/or thought leaders in the space talking about, and how we stay
relevant from their perspective.-->

The relevant analyst landscape here is the broader category of Project Management, so the [same remarks](https://gitlab.com/groups/gitlab-org/-/epics/666#analyst-landscape) from that category applies here as well.

## Top Customer Success/Sales issue(s)
<!-- These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

Improving the API of time tracking will allow customers to better manipulate time tracking data and allow customers to further customize their needs of time tracking. See https://gitlab.com/gitlab-org/gitlab-ce/issues/33395. Customers can already export time tracking data via issue CSV files. See https://docs.gitlab.com/ee/user/project/issues/csv_export.html.

## Top user issue(s)
<!-- This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

Users want additional customization with time tracking, since different teams evaluate and use time units slightly differently. See https://gitlab.com/gitlab-org/gitlab-ce/issues/30355.

Users also want more integration with other parts of GitLab, including integration with issue boards. See https://gitlab.com/gitlab-org/gitlab-ce/issues/27780.

## Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->

- Professional services need: https://gitlab.com/gitlab-org/gitlab-ee/issues/10741.

## Top Vision Item(s)
<!-- What's the most important thing to move your vision forward?-->

As mentioned above, we should get some quick wins with https://gitlab.com/gitlab-org/gitlab-ce/issues/30355 and https://gitlab.com/gitlab-org/gitlab-ce/issues/27780.