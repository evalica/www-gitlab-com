---
layout: markdown_page
title: "Career Development"
---

## On this page
{:.no_toc}

- TOC
{:toc}
## Tools

There are a number of tools we use to plot and manage career development:

* Role descriptions, which outline role responsibilities, requirements and nice-tohaves for each level in the role.
  * [DBRE](https://about.gitlab.com/job-families/engineering/database-reliability-engineer/)
  * [SRE](https://about.gitlab.com/job-families/engineering/site-reliability-engineer/)
  * [Backend Developer, Delivery](https://about.gitlab.com/job-families/engineering/backend-engineer/)
  * [Backend Developer, Infrastructure](https://about.gitlab.com/job-families/engineering/backend-engineer/)
* [Experience Factor Worksheets](https://about.gitlab.com/handbook/people-operations/global-compensation/#experience-factor), which reflects demonstrated ability to drive projects and deliverables from the position description in a quantifiable manner.
* Quarterly checkpoints
* 1:1s
* [360 Feedback](https://about.gitlab.com/handbook/people-operations/360-feedback/)

## Workflow

* **EFW**: All GitLabbers should have an EFW which reflects their current level's experience factor.
* **Quarterly Checkpoints**
  * On the first 1:1 meeting of the quarter, discuss the EFW and select two or three role responsibility areas to focus on during the quarter. Make a note of them in the 1:1 document and add them to the standing agenda. For each area, outline specific goals and targets to achieve, as well as ideas to work on them.
  * On the last 1:1 meeting of the quarter, discuss and record a summary of the progress made for the quarter and update the EFW accordingly.
* **1:1s**: Mentor on the focus areas in weekly 1:1s at a frequency that feels right for the tasks. There is no hard rule to discuss the career development items every week, but try to do so regularly and avoid a wide-open, ad-hoc conversation. 

There is no rule that guides the amount of progress that should be made in a given time-period in a strict fashion. We should however strive to set targets to progress to to the [next level](https://about.gitlab.com/handbook/people-operations/global-compensation/#explanation) on at least a quarterly basis.

Actions to make changes to a GitLabber's level can be taken during the [360 Feedback](https://about.gitlab.com/handbook/people-operations/360-feedback/), and the data collected throughout this workflow should be useful at that time.

### Managers

You must prepare to provide mentoring and guidance on focus areas for the quarter, especially in terms of how to improve and evolve in said areas. Do not hesitate to ask for help if you feel you need guidance to assess strategies for mentorship, and feel free to reach out to the other GitLabbers who can provide said guidance. Be clear and concise in setting expectations, trying to align them with work that will take place during the quarter.

### Individual Contributors

Think about the areas that are important to you and commmit to working on them over the quarter. Keep an open mind to receive feedback and guidance on these areas, and be an active participant in the process. 

## Guidelines and Considerations

Career development requires that all parties involved be commited to the process and are active participants.

One implicit area to focus on explicitly is **communication**. Regardless of your role, whether tehnical or manager, you must be a good communicator, and thus, it is important to always improve our communication skills.

While there is no time-boxing for level progress, the company does operate on a [specific cycle](https://about.gitlab.com/handbook/people-operations/360-feedback/) to make changes to a GitLabbers level during the [360 Feedback](https://about.gitlab.com/handbook/people-operations/360-feedback/).

