---
layout: markdown_page
title: "Roadmap"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# Overview

Our roadmap overview is separated into 2 sections, **[Track](#track)** and **[Timeline](#timeline)**.

The **[Track](#track)** section displays the type of work grouped by topics/themes. Each topic or theme is broken down into phases so they can be put on the timeline.

The **[Timeline](#timeline)** section shows in chronological order when we would expect to complete a particular phase of work.

A phase can be captured by an epic or a parent issue.

## Track

### Coverage

Work to improve the overall test coverage.

#### Testcase Management

A testcase management system to document all E2E tests and their types, and link to test reports.

* **Phase 1**: Basic [Google spreadsheet](https://docs.google.com/spreadsheets/d/1RlLfXGboJmNVIPP9jgFV5sXIACGfdcFq1tKd7xnlb74/edit) to plan type of meta data.
* **Phase 2**: Using issues as currently implemented to automate some testcase management tasks. A [proof of concept](https://gitlab.com/gitlab-org/quality/testcases) is available.
* **Phase 3**: Integration of test and code. Have tests reference testcase links so they can be updated/synced dynamically.
* **Phase 4**: Testcase Management as GitLab Native feature. [Epic](https://gitlab.com/groups/gitlab-org/-/epics/617)

#### Cross-browser tests

[Cross-browser and mobile test coverage and infrastructure.](https://gitlab.com/gitlab-org/quality/team-tasks/issues/45).

Issues: [Crossbrowser & Mobile Browser testing coverage and infrastructure](https://gitlab.com/gitlab-org/quality/team-tasks/issues/45)

* **Phase 1**: [Basic capability to run on other browsers besides chrome (IE11, Firefox)](https://gitlab.com/groups/gitlab-org/-/epics/609).
* **Phase 2**: Internet Explorer, and Edge coverage.
* **Phase 3**: Other desktop browsers.
* **Phase 4**: Mobile browser coverage.
* **Phase 5**: Run smoke test on soon to be new stable versions of important browsers to detect issues early.

#### Service-integration tests

Epic: [https://gitlab.com/groups/gitlab-org/-/epics/661](https://gitlab.com/groups/gitlab-org/-/epics/661)

* **Phase 1**: Basic tests for LDAP and SAML.
* **Phase 2**: Github and Google OAuth tests.
* **Phase 3**: Jenkins and Jira integration tests.

#### Visual-diff tests

Browser screenshot visual testing to catch visual bugs. Helps with validating layout, UX, and accessibility.

Issue: [End-to-end visual regression validation](https://gitlab.com/gitlab-org/quality/team-tasks/issues/47).

* **Phase 1**: Lean on 3rd party tools to get something running fast and learn from the product.
* **Phase 2**: Basic pixel to pixel comparison implemented as part of GitLab.
* **Phase 3**: Visual coverage smoke test for all stage groups.
* **Phase 4**: Use visual diff as a GitLab Native feature.

#### Performance tests

* **Phase 1**: Basic functional tests that creates a big issue and merge request.
* **Phase 2**: First stress test environment for on-prem customers with test runs and monitoring.
* **Phase 3**: Real 10,000 user reference architecture with customer reference traffic load testing.

#### Test planning

Test planning process.

* **Phase 1**: Roll out testplan format and planning process.
* **Phase 2**: Bake in test planning as part of feature planning process.
* **Phase 3**: Iterate and come up with a shorter version of the [10 min ACC framework](https://testing.googleblog.com/2011/09/10-minute-test-plan.html).

### Efficiency

Work that increases our efficiency and productivity.

#### Fault Tolerance

* **Phase 1**: Test retry.
* **Phase 2**: Dynamic Page Object locators validation.
* **Phase 3**: API client automatically retries on 5xx errors.

#### Faster Execution

Running test faster.

* **Phase 1**: Basic parallelization via runners.
* **Phase 2**: Parallelization at the process level for all E2E tests, exponential cost saving of CI runners.
* **Phase 3**: Run all tests at the same time, the whole suite takes only as long as the longest test.
* **Phase 4**: Evaluation of a subset of tests instead of running all the E2E tests depending on what changed.
  * Investigate the use of [running jobs when there are changes for a given path](https://gitlab.com/gitlab-org/gitlab-ce/issues/19232) for GitLab QA jobs.

#### API Usage

Use API in all E2E tests. Achieve optimal test layering with API suite more than %60 of total tests.

* **Phase 1**: [Use API calls to build the most used resources (Group, Project, User) in tests that are not focused on testing these resources behaviors](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests/21302). => Done
* **Phase 2**: Use API calls in login and logout for smoke tests.
* **Phase 3**: Implement API fabrication for all the resources that support it.
* **Phase 4**: Use API calls for setting up all resources in every E2E tests. Roll out as a standard for every new test.

#### Lean pyramid

Optimize test coverage across the layers of the test pyramid, to remove redundant tests and achieve higher coverage with greater efficiency.

* **Phase 1**: TBD


#### Test data

Come up with standardized test data that can be seeded in all environments for productivity.

* **Phase 1**: Test data curation, define a test datamodel which is static.
Define better project structure for ease of debugging, more readability in automated test data output, better group, project and issue naming (not just using timestamps).
* **Phase 2**: Script to setup testdata and clean them up.
  * Idempotent script based on API calls (E.g. adds project if missing, uses existing if exists).
* **Phase 3**: Setup 50% of planned test data from Phase 2 in [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit), Review Apps, Staging and Canary/Production.
* **Phase 4**: Setup 100% of planned test data from Phase 2 in [GDK](https://gitlab.com/gitlab-org/gitlab-development-kit), Review Apps, Staging and Canary/Production.

#### Test results

Work that will allow us to debug tests more easily. Includes better reporting and more informative artifacts.

* **Phase 1**: Better readability in test output.
* **Phase 2**: Basic HTML reporting.
* **Phase 3**: Automated reporting into the Testcase management system.
* **Phase 4**: Automated test name update via linking of issue ID in test code.

#### Test readability

* **Phase 1**: Standard Page Object method names for click navigation.
* **Phase 2**: TBD

### Triage

Work that will help us triage issues and merge requests more efficiently.

#### Triage packages

* **Phase 1**: Team level triage packages. => Done
* **Phase 2**: Group level triage packages.  => Done
* **Phase 3**: Summary report in group packages showing the amount of bugs for that group. Divide up into quadrants of severity and priority (S/P) labels.
* **Phase 4**: Bug SLA summary report in group packages.

#### Grooming

* **Phase 1**: Basic reminder for issues and merge requests. => Done
  * Merge requests that are open for a long time
  * Merge requests that do not have appropriate team and throughput labels.
  * Issues that are open for a long time (3 months / 6 months).
  * Merge requests that do not have any labels or milestones.
* **Phase 2**: Milestone grooming introduction. => Done
* **Phase 3**: Enforce one team label per merge request.
* **Phase 4**: Automatically infer team label from author.
* **Phase 5**: Automatic labelling via [natural language processing](https://en.wikipedia.org/wiki/Natural_language_processing) of issue description.
  * Investigate integrating [https://gitlab.com/tromika/gitlab-issues-label-classification](https://gitlab.com/tromika/gitlab-issues-label-classification) with GitLab Insights.

### Measure

Work involving metrics that will allow us to make good data-driven decisions and report them to stakeholders early.

#### Insights

* **Phase 1**: GitLab Insights prototype with initial metrics. => Done.
* **Phase 2**: Migrate GitLab Insights into GitLab.

#### Bug metrics

* **Phase 1**: Overall creation rate of bugs. => Done.
* **Phase 2**: Creation rate of bugs displayed in each group stage's dashboard.
* **Phase 3**: TBD

#### SLA metrics

* **Phase 1**: Mean time to resolve `P1` and `P2` defects.
* **Phase 2**: TBD

### Releases

Work that helps in validating the release process.

#### Review apps

* **Phase 1**: Shift QA tests to completely run against review apps including the orchestrated tests. Retiring the package-and-qa job.

## Timeline

### `CY2018Q4`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   | [Performance tests](#performance-tests) **Phase 1** |
| Efficiency |      |
| Triage     | [Triage Packages](#triage-packages) **Phase 1**, [Grooming](#grooming) **Phase 1** |
| Measure    | [Bug metrics](#bug-metrics) **Phase 1**, [Insights](#insights) **Phase 1**|
| Releases   |      |

### `FY2020Q1`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   | [Testcase Management](#testcase-management) **Phase 1**, [Performance tests](#performance-tests) **Phase 2** |
| Efficiency |      |
| Triage     | [Triage Packages](#triage-packages) **Phase 2** |
| Measure    | [Insights](#insights) **Phase 2**, [Grooming](#grooming) **Phase 2**|
| Releases   |      |

### `FY2020Q2`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   | [Cross-browser tests](#cross-browser-tests) **Phase 1**, [Performance tests](#performance-tests) **Phase 3** |
| Efficiency | [Faster execution](#faster-execution) **Phase 1**, [Test Results](#test-results) **Phase 1** |
| Triage     | [Triage Packages](#triage-packages) **Phase 3**, [Grooming](#grooming) **Phase 3** |
| Measure    | [SLA metrics](#sla-metrics) **Phase 1** |
| Releases   |      |

### `FY2020Q3`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   | [Testcase Management](#testcase-management) **Phase 2**, [Visual-diff tests](#visual-diff-tests) **Phase 1**, [Test planning](#test-planning) **Phase 2**, [Cross-browser tests](#cross-browser-tests) **Phase 2** |
| Efficiency | [Test data](#test-data) **Phase 1**, [Test data](#test-data) **Phase 2**, [Test Results](#test-results) **Phase 2** |
| Triage     | [Grooming](#grooming) **Phase 4** |
| Measure    |      |
| Releases   |      |


### `FY2020Q4`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   | [Visual-diff tests](#visual-diff-tests) **Phase 2**, Cross-browser tests](#cross-browser-tests) **Phase 3**|
| Efficiency | [Test data](#test-data) **Phase 3**, [Test data](#test-data) **Phase 4**, [Test Results](#test-results) **Phase 3** |
| Triage     | [Triage Packages](#triage-packages) **Phase 4** |
| Measure    |      |
| Releases   |      |


### `FY2021Q1`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   |      |
| Efficiency | [Test Results](#test-results) **Phase 4** |
| Triage     |      |
| Measure    |      |
| Releases   |      |


### `FY2021Q2`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   |      |
| Efficiency |      |
| Triage     |      |
| Measure    |      |
| Releases   |      |

### `FY2021Q3`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   |      |
| Efficiency |      |
| Triage     |      |
| Measure    |      |
| Releases   |      |

### `FY2021Q4`

|   Tracks   | Work |
| ---------- | ---- |
| Coverage   |      |
| Efficiency |      |
| Triage     |      |
| Measure    |      |
| Releases   |      |
