---
layout: markdown_page
title: "RM.1.04 - Service Risk Rating Assignment Control Guidance"
---

## On this page
{:.no_toc}

- TOC
{:toc}

# RM.1.04 - Service Risk Rating Assignment

## Control Statement

Quarterly, GitLab prioritizes the frequency of vulnerability discovery activities based on an assigned service risk rating.

## Context

The purpose of this control is to prioritize security resources and efforts to the services that benefit the most from them. By prioritizing services based on risk rating, we can allocate time and resource to the places that would most benefit from them.

## Scope

This control applies to all GitLab applications and services.

## Ownership

TBD

## Implementation Guidance

For detailed implementation guidance relevant to GitLabbers, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.04_service_risk_rating_assignment.md).

## Reference Links

For all reference links relevant to this control, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.04_service_risk_rating_assignment.md).

## Examples of evidence an auditor might request to satisfy this control

For examples of evidence an auditor might request, refer to the [full guidance documentation](https://gitlab.com/gitlab-com/gl-security/compliance/compliance/blob/master/controls/guidance/RM.1.04_service_risk_rating_assignment.md).

## Framework Mapping

* ISO
  * A.12.7.1
  * A.18.2.1
  * A.18.2.2
  * A.18.2.3
* SOC2 CC
  * CC1.2
  * CC3.2
  * CC3.4
  * CC4.1
  * CC4.2
  * CC5.1
  * CC5.2
