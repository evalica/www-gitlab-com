---
layout: job_family_page
title: "Technical Writing Manager"
---

## Responsibilities

* Managing the Technical Writing team.
* Maintain a single source of truth across our sites, documentation and tutorials.
* Deliver input on promotions, function changes, demotions and firings in consultation with the CEO, and VP of Product

### Documentation

* Manage documentation improvement efforts.
* Create practices that encourage keeping documentation up to date and easily discoverable.
* Make sure by the time of release all documentation, guides, and related content were shipped.
* It is not the responsibility of the Technical Writing team to write the first
documentation draft for new or updated features.
* Create tutorials on a workflow designed around GitLab's defining features.

### Website

* Work with the Sales and Marketing team leads to make sure the website product content is comprehensive and up to date.
* Create a self-managing framework for the Community Writers program.
* Grow the Remote Only and Conversational Development websites.

## Ongoing Projects

* Reorganize documentation around topics, grouping related content into topic
  portals with a topic index page to overview the included content, taking
  readers on journey from getting started as a beginner to advanced administration.
* Create marketing website pages comparing GitLab features vs popular competing products.
* Separate documentation rendering from the rails app. Either update GitLab's
  `/help` to point to `docs.gitlab.com` or local build of the static site documentation.
* Work with the VP of Product to empower PMs and Engineers to write the first
  draft of documentation for new or updated features. Use the release blog
  post as a changelog checklist to ensure everything is documented.

## Requirements

* Experience in setting up online documentation, this is a hard requirement for this role.
* Previous experience in using Git, this is a hard requirement for this role.
* Proven experience in managing, mentoring, and training teams of technical writers
* Experience working on production-level documentation
* Self-motivated and strong organizational skills
* Strong written and spoken communication skills
* Familiarity with Static Site Generators, HTML, CSS
* Familiarity with Continuous Integration, Containers, Kubernetes and Project Management software a plus
* Be able to work with the rest of the community
* You share our values, and work in accordance with those values
