---
layout: job_family_page
title: "Vice President of Product"
---

The VP of Product reports to our CEO. The key focus of this role is to define and execute a vision and roadmap company’s products. Also to facilitate strong collaboration between product, engineering, the CEO, and other executives. The VP of Product at GitLab will 
need to make sure that the company is building, shipping and supporting the right products. 

## Responsibilities

- Lead a team of product directors.
- Recruit, coach, and retain those directors.
- Ensure all our product managers are very effective.
- Capture the product vision.
- Allocation of engineering effort across categories (manage, plan, create, etc.)
- Allocation of engineering effort activities (new functionality, improve existing functionality, bugfix, security, availability, performance, technical debt.
- Determine needed engineering headcount and organization structure.
- Pricing tier decisions and process that sales and the wider community embrace.
- Customers feel understood.
- Promoting our product function.
- Actionable information to product marketing.
- Growth / user marketing strategy and implementation with the relevant product manager.
- Data strategy to measure usage in many ways that enables customer success, in product activation, and monetization.
- Ensure all of GitLabs features are used throughout our organization.
- Collaborate with UX to refine feature ideas upstream from implementation.
- Collaborate with our VP of engineering on turning ideas into shipped production features.
- Maintain a transparent, up-to-date roadmap.
- Deal with a high volume of suggestions from the CEO.
- Push back on bad and untimely CEO ideas.
- Collect competitive advantages and shortcomings in a structured way.
- Talk to customers regularly and provide that connection to the rest of the company.
- Be passionate about and use the product management features within GitLab like epics and portfolio management.
- Ensure the flow through the product is great.
- Manage the process by which ideas are transformed into detailed requirements on a prioritized backlog.
- Craft minimum viable changes (MVC’s) to be made iteratively to our platform.

## Requirements

- Experience hiring and managing a multi-level team of product managers and directors.
- Have a strong, articulate, and correct vision for the future of GitLab and current shortcomings.
- Articulate a clear, correct, and reproducible prioritization process.
- Enterprise software experience.
- Developer tool or platform industry experience.
- SaaS and self-managed (on-premises) experience.
- Experience with hyper-growth, more than 25% QoQ.
- Experience with growth strategy and data strategy.
- Ability to take decisions at high velocity.
- Able to define minimum viable changes.
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- Successful completion of a [background check](/handbook/people-operations/code-of-conduct/#background-checks).

## Links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)
